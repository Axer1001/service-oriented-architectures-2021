# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: mafia.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0bmafia.proto\"|\n\rStatusMessage\x12\x10\n\x08username\x18\x01 \x01(\t\x12)\n\x05\x65vent\x18\x02 \x01(\x0e\x32\x1a.StatusMessage.event_types\".\n\x0b\x65vent_types\x12\r\n\tCONNECTED\x10\x00\x12\x10\n\x0c\x44ISCONNECTED\x10\x01\"-\n\x0bRoleMessage\x12\x10\n\x08username\x18\x01 \x01(\t\x12\x0c\n\x04role\x18\x02 \x01(\t2\x8a\x01\n\x05Mafia\x12,\n\x08JoinGame\x12\x0e.StatusMessage\x1a\x0e.StatusMessage0\x01\x12,\n\nDisconnect\x12\x0e.StatusMessage\x1a\x0e.StatusMessage\x12%\n\x07GetRole\x12\x0c.RoleMessage\x1a\x0c.RoleMessageb\x06proto3')



_STATUSMESSAGE = DESCRIPTOR.message_types_by_name['StatusMessage']
_ROLEMESSAGE = DESCRIPTOR.message_types_by_name['RoleMessage']
_STATUSMESSAGE_EVENT_TYPES = _STATUSMESSAGE.enum_types_by_name['event_types']
StatusMessage = _reflection.GeneratedProtocolMessageType('StatusMessage', (_message.Message,), {
  'DESCRIPTOR' : _STATUSMESSAGE,
  '__module__' : 'mafia_pb2'
  # @@protoc_insertion_point(class_scope:StatusMessage)
  })
_sym_db.RegisterMessage(StatusMessage)

RoleMessage = _reflection.GeneratedProtocolMessageType('RoleMessage', (_message.Message,), {
  'DESCRIPTOR' : _ROLEMESSAGE,
  '__module__' : 'mafia_pb2'
  # @@protoc_insertion_point(class_scope:RoleMessage)
  })
_sym_db.RegisterMessage(RoleMessage)

_MAFIA = DESCRIPTOR.services_by_name['Mafia']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _STATUSMESSAGE._serialized_start=15
  _STATUSMESSAGE._serialized_end=139
  _STATUSMESSAGE_EVENT_TYPES._serialized_start=93
  _STATUSMESSAGE_EVENT_TYPES._serialized_end=139
  _ROLEMESSAGE._serialized_start=141
  _ROLEMESSAGE._serialized_end=186
  _MAFIA._serialized_start=189
  _MAFIA._serialized_end=327
# @@protoc_insertion_point(module_scope)
