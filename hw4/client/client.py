import enum
import mafia_pb2_grpc
import mafia_pb2
import grpc
import threading
from threading import Lock
import curses
import time
from enum import Enum

user_list_lock = Lock()
user_list = set()

my_role = ''
my_username = ''

class MafiaConst(Enum):
    GAME_USER_COUNT=6



def updateUserList(usersStream) -> None:
    
    for user in usersStream:
        if user.event == user.CONNECTED:
            with user_list_lock:
                user_list.add(user.username)
        
        elif user.event == user.DISCONNECTED:
            
            with user_list_lock:
                try:
                    user_list.remove(user.username)
                except:
                    pass

                

def screenRoutine() -> None:
    stdscr = curses.initscr()

    curses.noecho()

    while len(user_list) < MafiaConst.GAME_USER_COUNT.value:
        stdscr.clear()
        stdscr.addstr(20, 0, 'Game not started')
        stdscr.addstr(2, 40, 'Players:')

        user_list_lock.acquire()
        for i, username in enumerate(user_list):
            stdscr.addstr(3 + i, 46, f'{i+1}. {username}')
        
        stdscr.refresh()
        user_list_lock.release()
        time.sleep(0.2)
    
    stdscr.clear()
    stdscr.refresh()
    
    # stdscr.addstr(2, 40, 'Players:')
    # for i, username in enumerate(user_list):
    #         stdscr.addstr(3 + i, 46, f'{i+1}. {username}')
    
    # stdscr.addstr(1, 20, 'Game started!')
    # stdscr.refresh()
    # stdscr.getkey()

def gameRoutine(clientStub):
    response = clientStub.GetRole(mafia_pb2.RoleMessage(username=my_username))
    my_role = response.role

    stdscr = curses.initscr()

    curses.noecho()

    stdscr.addstr(1, 0, 'Game started!')
    stdscr.addstr(2, 40, 'Players:')
    for i, username in enumerate(user_list):
            stdscr.addstr(3 + i, 46, f'{i+1}. {username}')
    stdscr.addstr(3, 5, f'Your role: {my_role}')

    stdscr.refresh()

    stdscr.getkey()




def mainRoutine(host: str, port: str, username: str) -> None:
    channel = grpc.insecure_channel(f'{host}:{port}')
    stub = mafia_pb2_grpc.MafiaStub(channel)

    usersStream = stub.JoinGame(mafia_pb2.StatusMessage(username=username, event=0))
    
    updateThr = threading.Thread(target=updateUserList, args=[usersStream])
    updateThr.daemon = True

    try:
        updateThr.start()
        
        screenRoutine()

    except:
        stub.Disconnect(mafia_pb2.StatusMessage(username=username, event=1))

    updateThr.join()

    gameRoutine(stub)

def main() -> None:
    global my_username

    host = input('Enter host: ')
    port = input('Enter port: ')
    username = input('Enter your nickname: ')

    my_username = username

    print(f'Connecting to server {host}:{port} with nickname: {username}')

    mainRoutine(host, port, username)


if __name__ == '__main__':
    main()