import grpc
import mafia_pb2_grpc
import mafia_pb2
from concurrent import futures
from enum import Enum
import random
from threading import Lock

class MafiaConst(Enum):
    GAME_USER_COUNT=6

active_clients_q = []
disconnected_clients = []

active_clients = set()
active_clients_lock = Lock()

dataLock = Lock()
roles = ['mafia'] * 2 + ['innocent'] * 4
clients_roles = dict()

class MafiaServicer(mafia_pb2_grpc.MafiaServicer):

    def JoinGame(self, request, context):

        client_username = request.username

        print(f'Client {client_username} connected')
        
        active_clients_q.append(client_username)
        
        with active_clients_lock:
            active_clients.add(client_username)
        
        current_clients_it = 0
        
        for client in active_clients_q:
            yield mafia_pb2.StatusMessage(username=client, event=0)
        
        disconnected_clients_it = 0

        try:
            while len(active_clients) < MafiaConst.GAME_USER_COUNT.value or current_clients_it < len(active_clients_q):
                if current_clients_it < len(active_clients_q):
                    connected_client = active_clients_q[current_clients_it]
                    current_clients_it += 1
                    yield mafia_pb2.StatusMessage(username=connected_client, event=0)
                
                if disconnected_clients_it < len(disconnected_clients):
                    disconnected_client = disconnected_clients[disconnected_clients_it]
                    disconnected_clients_it += 1
                    yield mafia_pb2.StatusMessage(username=disconnected_client, event=1)
        except:   
            pass
    
    def Disconnect(self, request, context):
        global active_clients_count

        client_username = request.username
        
        disconnected_clients.append(client_username)
        
        with active_clients_lock:
            active_clients.remove(client_username)

        print(f'Client {client_username} disconnected!')

        return mafia_pb2.StatusMessage()
    
    def GetRole(self, request, context):
        
        global clients_roles, roles

        dataLock.acquire(blocking=True)

        if len(clients_roles) == 0:
            clients = list(active_clients)
            random.shuffle(clients)
            random.shuffle(roles)
            clients_roles = dict(zip(clients, roles))
        
        dataLock.release()


        return mafia_pb2.RoleMessage(username=request.username, role=clients_roles[request.username])




def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    mafia_pb2_grpc.add_MafiaServicer_to_server(MafiaServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()



def main():
    print('Starting server!')
    serve()


if __name__ == '__main__':
    main()