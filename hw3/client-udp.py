#!/usr/bin/python3
import os
import sys
import socket
import threading
from h11 import Data
from matplotlib.pyplot import disconnect
import pyaudio
from protocol import DataType, Protocol
import pickle
import curses
import time
import mute_alsa

class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.bufferSize = 4096
        self.connected = False
        self.name = input('Enter the name of the client --> ')
        self.idToName = {}
        self.nowSpeaking = ''
        while 1:
            try:
                self.target_ip = input('Enter IP address of server --> ')
                self.target_port = int(input('Enter target port of server --> '))
                self.server = (self.target_ip, self.target_port)
                self.connectToServer()
                break
            except:
                print("Couldn't connect to server...")

        chunk_size = 512
        audio_format = pyaudio.paInt16
        channels = 1
        rate = 20000

        # initialise microphone recording
        self.p = pyaudio.PyAudio()
        self.playing_stream = self.p.open(format=audio_format, channels=channels, rate=rate, output=True, frames_per_buffer=chunk_size)
        self.recording_stream = self.p.open(format=audio_format, channels=channels, rate=rate, input=True, frames_per_buffer=chunk_size)

        # start threads
        receive_thread = threading.Thread(target=self.receive_server_data)
        receive_thread.start()

        display_thread = threading.Thread(target=self.display)
        display_thread.start()


        self.send_data_to_server()
        receive_thread.join()
        display_thread.join()

    def receive_server_data(self):
        while self.connected:
            try:
                data, addr = self.s.recvfrom(1025)
                message = Protocol(datapacket=data)
                
                if message.DataType == DataType.ClientData:
                    self.nowSpeaking = self.idToName[message.head]
                    self.playing_stream.write(message.data)
                
                elif message.DataType == DataType.Handshake:
                    new_ids_to_names = pickle.loads(message.data)
                    
                    self.idToName = new_ids_to_names

            except:
                exit(0)

    def connectToServer(self):
        if self.connected:
            return True

        message = Protocol(dataType=DataType.Handshake, data=self.name.encode(encoding='UTF-8'))
        self.s.sendto(message.out(), self.server)

        data, addr = self.s.recvfrom(1025)
        datapack = Protocol(datapacket=data)

        if (addr==self.server and datapack.DataType==DataType.Handshake and 
        datapack.data.decode('UTF-8')=='ok'):
            print('Connected to server successfully!')
            self.connected = True
        return self.connected

    def send_data_to_server(self):
        while self.connected:
            try:
                data = self.recording_stream.read(512)
                message = Protocol(dataType=DataType.ClientData, data=data)
                self.s.sendto(message.out(), self.server)
            except:
                self.connected = False
                break
        
        disconnect_msg = Protocol(dataType=DataType.Handshake, data=b'')
        self.s.sendto(disconnect_msg.out(), self.server)

        self.s.close()
                
                
    def display(self):
        stdscr = curses.initscr()

        curses.noecho()

        while True:
            if self.nowSpeaking not in self.idToName.values():
                self.nowSpeaking = ''

            stdscr.clear()
            stdscr.addstr(20,0, f'Now talking: {self.nowSpeaking}')
            stdscr.addstr(2, 40, 'Users:')
            
            i = 0
            for id, user in self.idToName.items():
                stdscr.addstr(3 + i, 46, f'{id}. {user}')
                i += 1
            
            stdscr.refresh()
            time.sleep(0.2)

#unknown alsa errors
client = Client()
