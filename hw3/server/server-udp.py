#!/usr/bin/python3

import socket
import threading

from protocol import DataType, Protocol
import pickle

PORT_NUMBER = 5000

class Server:
    def __init__(self):
            self.ip = socket.gethostbyname(socket.gethostname())
            while 1:
                try:
                    self.port = 5000

                    self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    self.s.settimeout(5)
                    self.s.bind((self.ip, self.port))

                    break
                except:
                    print("Couldn't bind to that port")

            self.clients = {}
            self.clientCharId = {}
            threading.Thread(target=self.receiveData).start()

    def receiveData(self):   
        print('Running on IP: '+self.ip)
        print('Running on port: '+str(self.port))
        
        while True:
            try:
                data, addr = self.s.recvfrom(1025)
                message = Protocol(datapacket=data)
                self.handleMessage(message, addr)
                
            except:
                pass


    def handleMessage(self, message, addr):
        if self.clients.get(addr, None) is None:
            try:
                if message.DataType != DataType.Handshake:
                    return

                name = message.data.decode(encoding='UTF-8')

                self.clients[addr] = name
                self.clientCharId[addr] = len(self.clients)

                print('{} has connected on {}!'.format(name, addr))
                ret = Protocol(dataType=DataType.Handshake, data='ok'.encode(encoding='UTF-8'))
                self.s.sendto(ret.out(), addr)

                # update other clients info
                self.updateClientsInfo()
            except:
                pass
            return

        if message.DataType == DataType.Handshake:
            print('Client', self.clients[addr], 'disconnected!')
            
            self.clientCharId.pop(addr)
            self.clients.pop(addr)

            self.updateClientsInfo()
                

        if message.DataType == DataType.ClientData:
            self.broadcast(addr, message)
        


    def broadcast(self, sentFrom, data):
        data.head = self.clientCharId[sentFrom]
        for client in self.clients:
            if client != sentFrom:
                try:
                    self.s.sendto(data.out(), client)
                except:
                    pass
    
    def updateClientsInfo(self):
        ids_to_clients = {}
        for addr in self.clients.keys():
            ids_to_clients[self.clientCharId[addr]] = self.clients[addr]
        message = Protocol(dataType=DataType.Handshake, data=pickle.dumps(ids_to_clients))

        for client in self.clients:
            try:
                self.s.sendto(message.out(), client)
            except:
                print('ERROR')


server = Server()
