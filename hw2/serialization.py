from timeit import timeit
from setuptools import setup  
from tabulate import tabulate 
import sys  
 
message = '''
import random
d = { 
    'PackageID' : 1539, 
    'PersonID' : 33, 
    'Name' : """MEGA_GAMER_2222""",
    'Nums' : [i for i in range(10)],
    'Floats': [random.random() for i in range(7)], 
    'Inventory': dict((str(i), i) for i in iter(range(10))),   
    'CurrentLocation': """ 
        Pentos is a large port city, more populous than Astapor on Slaver Bay,  
        and may be one of the most populous of the Free Cities.  
        It lies on the bay of Pentos off the narrow sea, with the Flatlands  
        plains and Velvet Hills to the east. 
        The city has many square brick towers, controlled by the spice traders.  
        Most of the roofing is done in tiles. There is a large red temple in  
        Pentos, along with the manse of Illyrio Mopatis and the Sunrise Gate  
        allows the traveler to exit the city to the east,  
        in the direction of the Rhoyne. 
        """ 
}''' 

message_proto = """
import message_pb2
def fillMessage(d) -> message_pb2.Message:
    m = message_pb2.Message(
        PackageId = d['PackageID'],
        PersonId = d['PersonID'],
        Name = d['Name'],
        Nums = d['Nums'],
        Floats = d['Floats'],
        Inventory = d['Inventory'],
        CurrentLocation = d['CurrentLocation']
    )
    return m
%s""" % message

message_avro = """
from fastavro import schemaless_writer, schemaless_reader
from fastavro.schema import load_schema
from io import BytesIO
fo = BytesIO()
fi = BytesIO()
parsed_schema = load_schema('schema.avsc')
%s""" % message

setup_pickle    = '%s ; import pickle ; src = pickle.dumps(d, 2)' % message 
setup_json      = '%s ; import json; src = json.dumps(d)' % message 
setup_protobuf  = '%s ; import message_pb2; filledObject = fillMessage(d); src = filledObject.SerializeToString();' % message_proto
setup_xml       = '%s ; import dicttoxml; import xmltodict; src = dicttoxml.dicttoxml(d)' % message
setup_msgpack   = '%s ; import msgpack; src = msgpack.dumps(d)' % message
setup_yaml      = '%s ; import yaml; src = yaml.dump(d)' % message
setup_avro      = '%s ; schemaless_writer(fi, parsed_schema, d); fi.seek(0); src = fi.getvalue()' % message_avro

tests = [ 
#   (title, setup, enc_test, dec_test) 
    ('pickle (native serialization)', 'import pickle; %s' % setup_pickle, 'pickle.dumps(d, 2)', 'pickle.loads(src)'), 
    ('json', 'import json; %s' % setup_json, 'json.dumps(d)', 'json.loads(src)'), 
    ('protobuf', 'import message_pb2; %s' % setup_protobuf, 'filledObject.SerializeToString()', 'filledObject.ParseFromString(src)'),
    ('xml', setup_xml, 'dicttoxml.dicttoxml(d)', 'xmltodict.parse(src)'),
    ('msgpack', setup_msgpack, 'msgpack.dumps(d)', 'msgpack.loads(src)'),
    ('yaml', setup_yaml, 'yaml.dump(d)', 'yaml.full_load(src)'),
    ('avro', setup_avro, 'schemaless_writer(fo, parsed_schema, d)', 'schemaless_reader(fi, parsed_schema); fi.seek(0)')
] 
  
loops = 5000 
enc_table = [] 
dec_table = [] 
  
print ("Running tests (%d loops each)" % loops)

for title, mod, enc, dec in tests: 
    print (title) 
  
    print ("  [Encode]", enc)  
    result = timeit(enc, mod, number=loops) 
    exec (mod)
    enc_table.append([title, result, sys.getsizeof(src)]) 

    print ("  [Decode]", dec)  
    result = timeit(dec, mod, number=loops) 
    dec_table.append([title, result]) 
  
enc_table.sort(key=lambda x: x[1]) 
enc_table.insert(0, ['Package', 'Seconds', 'Size']) 
  
dec_table.sort(key=lambda x: x[1]) 
dec_table.insert(0, ['Package', 'Seconds']) 
  
print ("\nEncoding Test (%d loops)" % loops) 
print (tabulate(enc_table, headers="firstrow")) 
  
print ("\nDecoding Test (%d loops)" % loops) 
print (tabulate(dec_table, headers="firstrow")) 