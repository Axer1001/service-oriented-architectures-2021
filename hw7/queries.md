### GraphQl query examples

#### Method GetGame
```
query get {
  getGame(id: "1") {
    success
    errors
    game {
      is_active
      players
      player_scores
      game_comments
    }
  }
}
```
#### Method GetGamesHistory
```
query get {
  getGamesHistory {
    success
    errors
    game {
      is_active
      players
      player_scores
      game_comments
    }
  }
}
```

#### Method GetActiveGames
```
query get {
  getActiveGames {
    success
    errors
    game {
      is_active
      players
      player_scores
      game_comments
    }
  }
}
```

#### Method AddComment
```
mutation mut {
  AddComment(id: "1", text: "such a cool game") {
    success
    errors
    game {
      is_active
      players
      player_scores
      game_comments
    }
  }
}
```