import requests


class Game:
    def __init__(self, id, players, player_scores, game_comments):
        self.id = id
        self.players = players
        self.player_scores = player_scores
        self.game_comments = game_comments


def run_query(query):
    answer = requests.post('http://localhost:5000/graphql', json={'query':query})

    if answer.status_code == 200:
        return answer.json()
    else:
        raise Exception("Query failed to run by returning code of {}. {}".format(answer.status_code, query))


def parseGame(json_text, id=None):
    if not id:
        id = json_text["id"]
    game = Game(
        id,
        json_text["players"],
        json_text["player_scores"],
        json_text["game_comments"]
    )
    return game

def getGame(id):
    ans = run_query(getGameQuery.format(id))
    return parseGame(ans["data"]["getGame"]["game"], id=id)

def getActiveGames():
    ans = run_query(getActiveGamesQuery)
    games = []
    for game_json in ans["data"]["getActiveGames"]["game"]:
        games.append(parseGame(game_json))
    return games

def getGamesHistory():
    ans = run_query(getGamesHistoryQuery)
    games = []
    for game_json in ans["data"]["getGamesHistory"]["game"]:
        games.append(parseGame(game_json))
    return games


def addComment(id, text):
    ans = run_query(addCommentToGameMutation.format(id, text))
    return parseGame(ans["data"]["AddComment"]["game"])


getActiveGamesQuery = """
{
  getActiveGames {
    success
    errors
    game {
      id
      players
      player_scores
      game_comments
    }
  }
}
"""

getGamesHistoryQuery = """
{
  getGamesHistory {
    success
    errors
    game {
      id
      players
      player_scores
      game_comments
    }
  }
}
"""

getGameQuery = """
{{
  getGame(id: "{0}") {{
    success
    errors
    game {{
      players
      player_scores
      game_comments
    }}
  }}
}}
"""


addCommentToGameMutation = """
mutation Comments {{
  AddComment(id: "{0}", text: "{1}") {{
    success
    errors
    game {{
      id
      players
      player_scores
      game_comments
    }}
  }}
}}
"""
