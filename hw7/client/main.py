import curses
from queries import Game
from queries import getActiveGames, getGame, getGamesHistory, addComment
import time
from prettytable import PrettyTable


stdscr = curses.initscr()
curses.noecho()
curses.cbreak()

def view_active_games_screen():
    stdscr.clear()

    t = "### Active games screen ###\n"
    stdscr.addstr(2, curses.COLS // 2 - len(t) // 2, t)

    games = getActiveGames()

    table = PrettyTable()
    table.field_names = ["Game ID", "Number of players", "Top score"]

    for game in games:
        table.add_row([game.id, len(game.players), max(game.player_scores)])
    
    i = 0
    for row in table.get_string().split('\n'):
        stdscr.addstr(7 + i, curses.COLS // 2 - len(row) // 2, row)
        i += 1

    stdscr.addstr(20, 1, "Press any key to return")
    stdscr.getkey()

def view_game_history_screen():
    stdscr.clear()

    t = "### Games history screen ###\n"
    stdscr.addstr(2, curses.COLS // 2 - len(t) // 2, t)

    games = getGamesHistory()

    table = PrettyTable()
    table.field_names = ["Game ID", "Number of players", "Top score"]
 
    for game in games:
        table.add_row([game.id, len(game.players), max(game.player_scores)])
    
    i = 0
    for row in table.get_string().split('\n'):
        stdscr.addstr(7 + i, curses.COLS // 2 - len(row) // 2, row)
        i += 1

    stdscr.addstr(20, 1, "Press any key to return")
    stdscr.getkey()

def view_game_screen():
    
    stdscr.clear()

    t = "### Game view screen ###\n"
    stdscr.addstr(2, curses.COLS // 2 - len(t) // 2, t)

    curses.echo()
    curses.nocbreak()

    stdscr.addstr(5, 3, "Enter game id: ")
    
    game_id = int(stdscr.getstr())

    curses.noecho()
    curses.cbreak()
    
    while True:
        game = getGame(game_id)

        stdscr.addstr(6, 3, "----------------------------")
        
        i = 0
        scoreboard = list(zip(game.players, game.player_scores))
        scoreboard.sort(key=lambda x: -x[1])
        for player, score in scoreboard:
            stdscr.addstr(7 + i, 3, f'{i+1}. {player} with {score} points')
            i += 1
        
        stdscr.addstr(7 + i, 3, f'---------Comments---------')
        i += 1
        
        for j, comment in enumerate(game.game_comments):
            stdscr.addstr(7 + i, 3, f'{j+1}. {comment}')
            i += 1
        
        stdscr.addstr(20, 1, "Press any key to return")
        stdscr.refresh()

        stdscr.nodelay(True)
        ch = stdscr.getch()
        time.sleep(0.5)

        if ch != -1:
            break

    
    stdscr.nodelay(False)
    

def add_comment_to_game_screen():
    stdscr.clear()

    t = "### Add comments screen ###\n"
    stdscr.addstr(2, curses.COLS // 2 - len(t) // 2, t)

    curses.echo()
    curses.nocbreak()

    stdscr.addstr(5, 3, "Enter game id: ")
    game_id = int(stdscr.getstr())
    stdscr.addstr(6, 3, "Enter comment: ")
    comment = stdscr.getstr().decode()

    curses.noecho()
    curses.cbreak()

    addComment(game_id, comment)

    stdscr.addstr(8, 5, "Comment added!")
    stdscr.addstr(20, 1, "Press any key to return")
    stdscr.getkey()


def start_screen():
    stdscr.clear()
    
    t = "### Scoreboard viewer app ###\n"
    stdscr.addstr(2, curses.COLS // 2 - len(t) // 2, t)

    stdscr.addstr(5, 3, "Choose option:")
    stdscr.addstr(7, 4, "1. View active games")
    stdscr.addstr(8, 4, "2. View game history")
    stdscr.addstr(9, 4, "3. View game")
    stdscr.addstr(10, 4, "4. Add comment to game")
    stdscr.addstr(12, 5, "Press q to quit")

    stdscr.addstr(15, 3, "Choice [1-4 / q]: ")
    curses.echo()

    stdscr.refresh()

    key = stdscr.getkey()
    curses.noecho()
    

    if key == '1':
        view_active_games_screen()
    elif key == '2':
        view_game_history_screen()
    elif key == '3':
        view_game_screen()
    elif key == '4':
        add_comment_to_game_screen()
    else:
        curses.endwin()
        exit(0)


def main():
    while True:
        start_screen()


if __name__ == '__main__':
    main()