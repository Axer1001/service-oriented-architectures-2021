CREATE TABLE players (
    player_id SERIAL PRIMARY KEY,
    player_name VARCHAR(30),
    player_score INTEGER
);

CREATE TABLE game_records (
    game_id SERIAL PRIMARY KEY,
    player_ids integer[5],
    player_scores integer[5],
    game_comments VARCHAR[10]
);

INSERT INTO players(player_name, player_score) VALUES ('Vasya', 1);
INSERT INTO players(player_name, player_score) VALUES ('Petya', 2);
INSERT INTO players(player_name, player_score) VALUES ('MLGplay3r', 3);

INSERT INTO game_records(player_ids, player_scores) VALUES ('{1,2,3}', '{1,2,3}');
