from api import db
from api.models import Game
import numpy as np

db.create_all()

possible_players = ["Danya", "Nastya", "Ilya", "Anton", "Michael", "Jack", "Leo", "Anonymous"]
possible_comments = ["ggwp", "good game", "didnt like", "bad game", "had fun"]
for _ in range(4):
    num_players = np.random.randint(2, 5)
    num_comments = np.random.randint(0, 4)

    players = np.random.choice(possible_players, num_players, replace=False)
    player_scores = list(map(int, np.random.randint(0, 100, num_players)))
    comments = np.random.choice(possible_comments, num_comments)

    print(players, player_scores, comments)
    generated_game = Game(
        players=players,
        player_scores = player_scores,
        game_comments=comments,
        is_active=bool(np.random.randint(0, 2))
    )

    db.session.add(generated_game)


db.session.commit()