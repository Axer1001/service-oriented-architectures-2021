from api import db

class Game(db.Model):
    game_id = db.Column(db.Integer, primary_key=True)
    players = db.Column(db.ARRAY(db.String))
    player_scores = db.Column(db.ARRAY(db.Integer))
    game_comments = db.Column(db.ARRAY(db.String(100)))
    is_active = db.Column(db.Boolean)

    def to_dict(self):
        return {
            "id" : self.game_id,
            "players" : self.players,
            "player_scores" : self.player_scores,
            "game_comments" : self.game_comments,
            "is_active" : self.is_active
        }

