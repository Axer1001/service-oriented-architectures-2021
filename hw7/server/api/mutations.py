from ariadne import convert_kwargs_to_snake_case
from api import db
from api.models import Game

@convert_kwargs_to_snake_case
def AddComment_resolver(obj, info, id, text):
    try:
        game = Game.query.get(int(id))
        
        if game:
            if not game.game_comments:
                game.game_comments = []
            game.game_comments = game.game_comments  + [text]
        
        db.session.add(game)
        db.session.commit()
        payload = {
            "success": True,
            "game": game.to_dict()
        }
    except AttributeError:  # gamerecord not found
        payload = {
            "success": False,
            "errors": ["item matching id {id} not found"]
        }
    return payload