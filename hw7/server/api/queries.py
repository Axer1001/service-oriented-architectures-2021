from .models import Game

def GameRecords_resolver(obj, info):
    try:
        games = [game.to_dict() for game in Game.query.all()]
        print(games)
        payload = {
            "success": True,
            "game" : games
        }
    except Exception as error:
        print(error)
        payload = {
            "success" : False,
            "errors" : [str(error)]
        }
    return payload

def GameRecord_resolver(obj, info, id):
    try:
        game = Game.query.get(id)
        payload = {
            "success" : True,
            "game" : game
        }
    except Exception as error:
        print(error)
        payload = {
            "success" : False,
            "errors" : [str(error)]
        }
    return payload

def ActiveGameRecord_resolver(obj, info):
    try:
        games = [game.to_dict() for game in Game.query.filter_by(is_active=True).all()]
        print(games)
        payload = {
            "success": True,
            "game" : games
        }
    except Exception as error:
        print(error)
        payload = {
            "success" : False,
            "errors" : [str(error)]
        }
    return payload

def HistoryGameRecord_resolver(obj, info):
    try:
        games = [game.to_dict() for game in Game.query.filter_by(is_active=False).all()]
        print(games)
        payload = {
            "success": True,
            "game" : games
        }
    except Exception as error:
        print(error)
        payload = {
            "success" : False,
            "errors" : [str(error)]
        }
    return payload