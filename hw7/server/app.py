from api import app, db
from ariadne import load_schema_from_path, make_executable_schema, \
    graphql_sync, snake_case_fallback_resolvers, ObjectType
from ariadne.constants import PLAYGROUND_HTML
from flask import request, jsonify
from api.queries import GameRecords_resolver, GameRecord_resolver, ActiveGameRecord_resolver, HistoryGameRecord_resolver
from api.mutations import AddComment_resolver

query = ObjectType("Query")
query.set_field("getScoreBoard", GameRecords_resolver)
query.set_field("getGamesHistory", HistoryGameRecord_resolver)
query.set_field("getActiveGames", ActiveGameRecord_resolver)
query.set_field("getGame", GameRecord_resolver)

mutation = ObjectType("Mutation")
mutation.set_field("AddComment", AddComment_resolver)

type_defs = load_schema_from_path("schema.graphql")
schema = make_executable_schema(
    type_defs, query, mutation, snake_case_fallback_resolvers
)


@app.route("/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200

@app.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )
    status_code = 200 if success else 400
    return jsonify(result), status_code


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)