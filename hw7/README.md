### GraphQl WebService

#### GraphQl API
API endpoint: localhost:5000/graphql

Query examples [here](./queries.md)

#### Usage
Start database and server, this will also generate some game data and put it in database.
```
docker-compose up postgres
docker-compose up server
```
Install requirements
```
pip install -r requirements.txt
```
Start client
```
python main.py
```

#### Interface demonstration

##### Main page
![Main](./images/main.png)
##### Active games page
![Active games](./images/active-games.png)
##### Games history page
![Games history](./images/game-history.png)
##### Add comment page
![Add comment](./images/comment-add.png)
##### View game page
![View game](./images/game-view.png)

